﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Mementos
{
    public sealed class Memento
    {

        private readonly Stack<MementoEntry> undoStack = new Stack<MementoEntry>();
        private readonly Stack<MementoEntry> redoStack = new Stack<MementoEntry>();

        public bool CanUndo() {
            return undoStack.Count > 0;
        }

        public bool CanRedo() {
            return redoStack.Count > 0;
        }

        public void AddCommand(MementoEntry command)
        {
            undoStack.Push(command);

            while (redoStack.Count > 0) {
                var redo = redoStack.Pop();
                redo.Release();
            }
        }

        public void Clear() {
            while (redoStack.Count > 0)
            {
                var redo = redoStack.Pop();
                redo.Release();
            }

            while (undoStack.Count > 0)
            {
                var undo = undoStack.Pop();
                undo.Release();
            }
        }

        public void Redo()
        {
            if (redoStack.Count == 0)
            {
                return;
            }
            var command = redoStack.Pop();
            command.Redo();
            undoStack.Push(command);
        }

        public void Undo()
        {
            if (undoStack.Count == 0)
            {
                return;
            }
            var command = undoStack.Pop();
            command.Undo();
            redoStack.Push(command);
        }
    }
}

