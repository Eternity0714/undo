﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mementos
{
    public abstract class MementoEntry
    {
        public void Redo() {
            OnRedo();
        }

        public void Undo() {
            OnUndo();
        }

        public void Release() {
            OnRelease();
        }

        protected abstract void OnRedo();

        protected abstract void OnUndo();

        protected abstract void OnRelease();
    }

}

